
## 0.2.7 [11-20-2023]

* remove images from readme

See merge request itentialopensource/pre-built-automations/servicenow-create-change-request!25

---

## 0.2.6 [09-22-2023]

* Fix repolink in meta for replacement

See merge request itentialopensource/pre-built-automations/servicenow-create-change-request!24

---

## 0.2.5 [06-21-2023]

* Merging pre-release/2023.1 into master to run cypress tests

See merge request itentialopensource/pre-built-automations/servicenow-create-change-request!19

---

## 0.2.4 [05-22-2023]

* Merging pre-release/2022.1 into master to run cypress tests

See merge request itentialopensource/pre-built-automations/servicenow-create-change-request!17

---

## 0.2.3 [12-21-2021]

* Patch/dsup 1046

See merge request itentialopensource/pre-built-automations/servicenow-create-change-request!15

---

## 0.2.2 [07-09-2021]

* Certify on IAP 2021.1

See merge request itentialopensource/pre-built-automations/servicenow-create-change-request!14

---

## 0.2.1 [03-05-2021]

* Patch/lb 515

See merge request itentialopensource/pre-built-automations/servicenow-create-change-request!13

---

## 0.2.0 [06-18-2020]

* [minor/LB-404] Add './' to img path

See merge request itentialopensource/pre-built-automations/servicenow-create-change-request!5

---

## 0.1.0 [06-17-2020]

* Update manifest to reflect gitlab name

See merge request itentialopensource/pre-built-automations/ServiceNow-Create-Change-Request!4

---

## 0.0.2 [05-29-2020]

* Update bundles/workflows/ServiceNow Create Change - Automation Catalog.json,...

See merge request itentialopensource/pre-built-automations/staging/ServiceNow-Create-Change-Request!3

---\n\n\n\n
