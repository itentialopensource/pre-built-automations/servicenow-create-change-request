<!-- This is a comment in md (Markdown) format, it will not be visible to the end user -->
## _Deprecation Notice_
This Pre-Built has been deprecated as of 08-24-2023 and will be end of life on 06-30-2024. The capabilities of this Pre-Built have been replaced by the [Itential ServiceNow - Now Platform - REST Workflow Project](https://gitlab.com/itentialopensource/pre-built-automations/servicenow-now-platform-rest)

<!-- Update the below line with your pre-built name -->
# ServiceNow Create Change Request

<!-- Leave TOC intact unless you've added or removed headers -->
## Table of Contents

- [Overview](#overview)
  - [Operations Manager and JSON-Form](#operations-manager-and-json-form)
  - [Main Workflow](#main-workflow)
  - [Error Handling](#error-handling)
- [Installation Prerequisites](#installation-prerequisites)
- [Requirements](#requirements)
- [Features](#features)
- [How to Install](#how-to-install)
- [How to Run](#how-to-run)
  - [Input variables](#input-variables)
- [Additional Information](#additional-information)

## Overview

This pre-built integrates with the [ServiceNow Open Source Adapter](https://gitlab.com/itentialopensource/adapters/itsm-testing/adapter-servicenow) to create a Change Request in the `change_request` table in ServiceNow.

### Operations Manager and JSON-Form

This workflow has an [Operations Manager Item](ServiceNow-Create-Change-Request/bundles/automations/ServiceNow%20Create%20Change.json) that calls a workflow. The Ops Manager Item uses a JSON-Form to specify common fields populated when a Change Request is created. The workflow the Ops Manager Item calls queries data from the `formData` job variable.

### Main Workflow

The main workflow in this pre-built is the [ServiceNow Create Change](ServiceNow-Create-Change-Request/bundles/workflows/ServiceNow%20Create$20Change.json) workflow, which takes in several input variables required to create a change.

_Estimated Run Time_: 30 seconds

### Error Handling

If any task in the workflow fails to perform its expected function, the `taskError` job variable will hold the error message. If you are using any workflow in this pre-built as a childJob, you can use the `eval` task in Workflow Engine to check if the value of `taskError` is empty. If it is not empty, then the ServiceNow change that was created encountered an error.

## Installation Prerequisites

Users must satisfy the following pre-requisites:

- Itential Automation Platform
  - `^2023.1`
- [ServiceNow Open Source Adapter](https://gitlab.com/itentialopensource/adapters/itsm-testing/adapter-servicenow)
  - `^2.2.0`

## Requirements

This pre-built requires the following:

- A [ServiceNow Open Source Adapter](https://gitlab.com/itentialopensource/adapters/itsm-testing/adapter-servicenow).
- A ServiceNow account that has write access to the `change_request` table.

## Features

The main benefits and features of the pre-built are outlined below.

* Provide the fields required to create a ServiceNow Change.
* Query the Change Number after the Change is created.

## How to Install

To install the pre-built:

- Verify you are running a supported version of the Itential Automation Platform (IAP) as listed above in the [Requirements](#requirements) section in order to install the pre-built. 
- The pre-built can be installed from within Pre-builts catalog. Simply search for the name of your desired pre-built and click the install button.

## How to Run

Use the following to run the pre-built:

* Run the Operations Manager Item `ServiceNow Create Change Request` or call [ServiceNow Create Change](ServiceNow-Create-Change-Request/bundles/workflows/ServiceNow%20Create$20Change.json)  from your workflow as a childJob.

### Input Variables

_Example_

```json
  {
    "short_description": "a short description",
    "description": "a longer description \n can be multiline",
    "start_date": "2099-04-29 13:05:12",
    "end_date": "2099-04-30 13:05:12",
    "assigned_to": "Ldap Username",
    "cmdb_ci": "ci name",
    "implementation_plan": "plan \n can be multiline",
    "test_plan": "plan \n can be multiline",
    "backout_plan": "plan \n can be multiline",
  }
```

##Additional Information

Please use your Itential Customer Success account if you need support when using this pre-built.
